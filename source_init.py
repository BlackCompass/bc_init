from sys import argv
import os.path
import json



var_keys = ["filename","extension","author","include_main","include_headers","headers","main","brief","description"]
args = ["-f","-ext","-a","-im","-ih","-h","-M","-b","-d"]
examples = ["-f new_file","-ext .c","-a \"BlackCompass\"","-im True","-ih True","-h 2 math.h stdio.h","-M \"int main(void){\\n return 0 }",
                                                                                           "-b \"this program creates others\"",
            "-d \"This is a very long description\""]



def parse_default(default = ".default_file_args.json"):

    if default == ".default_file_args.json":
        with open(default) as f:
            jdefault = json.load(f)
    else:
        jdefault = json.loads(default)

    global _file_name,_file_extension,_author,_include_main,_include_headers,_headers,_main,_description,_brief

    _file_name,_file_extension = jdefault["filename"] , jdefault["extension"]
    _author ,_brief = jdefault ["author"] , jdefault ["brief"]
    _include_headers,_headers = jdefault["include_headers"],jdefault ["headers"]
    _include_main = jdefault["include_main"]
    _main = jdefault["main"]
    _description = jdefault["description"]

def write_current():

    with open(".default_file_args.json","+w") as f:
        jdefault = dict()

        jdefault ["filename"],jdefault["extension"] = _file_name , _file_extension
        jdefault ["author"],jdefault["brief"] = _author,_brief
        jdefault ["include_headers"],jdefault["headers"] = _include_headers,_headers
        jdefault ["include_main"] = _include_main
        jdefault ["main"] = _main
        jdefault ["description"] = _description

        json.dump(jdefault,f)

def get_default():

    global _file_name, _file_extension, _author, _include_main, _include_headers, _headers, _main, _brief, _description

    _file_name, _file_extension = "new_file", ".c"
    _author, _brief = "author", "brief"
    _include_main, _include_headers, _headers = False, True, ["stdio.h"]
    _main = "\n\nint main(int argc, char** argv){\n\treturn 0\n}"
    _description = "short description"

def parse_args():
    default_var = [_file_name, _file_extension, _author, _include_main, _include_headers, _headers, _main, _brief, _description]
    arguments = zip(args,default_var,var_keys)
    result = dict()
    for arg,var,key in arguments:
        if arg in argv:
            if arg == "-h":

                try:
                    header_sum = argv[argv.index("-h") + 1]
                    var_headers = []
                    for x in range(int(header_sum)):
                        var_headers.append(argv[(argv.index("-h") + 2) + x])
                    result[key] = var_headers
                    pass
                except IndexError:
                    print("Total Headers or Header argument not found, using default:",var)
                    result[key] = var
                    pass
                except TypeError:
                    print("Invalid header argument provided, using default:",var)
                    result[key] = var
                    pass

            else:
                try:
                    result[key] = argv[argv.index(arg) + 1]
                except IndexError:
                    print(key," argument not provided,using default: ",var)
                    result[key] = var
                    pass
        else:
            result[key] = var
    parse_default(json.dumps(result))


def compile_text():
    var_output = "/** @file %_f\n *  @brief %_b\n * \n *  %_d\n *  @author %_a \n *  @bug No known bugs. \n */\n"
    replacements = zip(["%_f","%_b","%_d","%_a"],[_file_name+_file_extension,_brief,_description,_author])

    for old,new in replacements:
        var_output = var_output.replace(old,new)

    return var_output


def printHstr():

    print("\nformat:\npython source_init.py <argument1> <argument parameter>...")
    for arg,key,example in zip(args,var_keys,examples):
        if arg == "-h":
            arg = "-h <sum>"
        print(("Type "+arg).ljust(15)+(" to set : " + key).ljust(30)+("ex: "+example).ljust(20))


    print("\nType #set-default".ljust(20)+"to set defaults and terminate. ".ljust(35)
          +"ex: Python source_init.py -a \"BlackCompass\" #set-default")
    print("Type  set-default".ljust(19) + "to set defaults and create a file. ".ljust(30)
          + "ex: Python source_init.py -a \"BlackCompass\" set-default\n")

if __name__=="__main__":

    if "-H" in argv:
        printHstr()
        exit(0)

    if os.path.exists("./.default_file_args.json"):
        parse_default()
    else:
        get_default()
        write_current()

    if len(argv) > 1:
        parse_args()

    if "#set-default" in argv:
        write_current()
        exit(0)
    else:
        if "set-default" in argv:
            write_current()
        with open(_file_name+_file_extension,"+w") as fileio:

            fileio.write( compile_text() )

            for header in _headers:
                fileio.write("#include <"+str(header)+">\n")
            if _include_main:
                fileio.write(_main)

    pass








